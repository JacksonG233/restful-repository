package;

import php.Lib;
import php.Web;
import sys.db.Mysql;
import sys.io.File;
import haxe.Json;

class Main {
	static public function main() {
		var zip = php.Web.getMultipart(999999999);
		if (zip["name"] == "form0") {
			sys.io.File.saveContent("/var/www/html/" + zip["id"] + ".zip", zip["clientZip"]);

			var clientZip = haxe.Json.stringify({
				id: zip["id"],
				title: zip["title"],
				summary: zip["summary"],
				description: zip["description"],
				howItWorks: zip["howItWorks"],
				imageUrl: zip["imageUrl"],
				videoUrl: zip["videoUrl"],
				version: zip["version"],
				domain: zip["domain"]
			});
			sys.io.File.saveContent("/var/www/html/jsons/" + zip["id"] + ".json", clientZip);

			var cnx = sys.db.Mysql.connect({
				user: "root",
				socket: null,
				port: 3306,
				pass: "rK4Z5wyIHp",
				host: "localhost",
				database: "test"
			});
			var path = "/var/www/html/" + zip["id"];
			var id = zip["id"];
			var sql = cnx.request('INSERT INTO zips (name, path) VALUES ("$id", "$path")');

			var sql = cnx.request('SELECT name FROM zips');
			var jasonList = new Array();
			for (row in sql) {
				jasonList.push(row.name);
			}
			var jasonList = haxe.Json.stringify(jasonList);
			sys.io.File.saveContent("/var/www/html/list.json", jasonList);
			php.Web.redirect("http://www.client-request-api.getforge.io");
		} else {
			if (zip["name"] == "form1") {
				var jasonList = haxe.Json.parse(sys.io.File.getContent("/var/www/html/jsons/" + zip["zipId"] + ".json"));
				Reflect.setField(jasonList, zip["newElement"], zip["newElementValue"]);
				var jasonList = haxe.Json.stringify(jasonList);
				sys.io.File.saveContent("/var/www/html/jsons/" + zip["zipId"] + ".json", jasonList);
				php.Web.redirect("http://www.client-request-api.getforge.io");
			} else {
				sys.io.File.saveContent("/var/www/html/" + zip["zipId"] + ".zip", zip["clientZip"]);
				php.Web.redirect("http://www.client-request-api.getforge.io");
			}
		}
	}
}
